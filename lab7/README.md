
Prolog has truly gained my respect as a language.

Processes | Execution time (ms) | Speedup | Efficiency
---|---|---|---
1 | 38402 | 1x | 25%
2 | 29298 | 1.31x | 33%
3 | 25690 | 1.49x | 37%
4 | 19123 | 2.00x | 50%
5 | 22071 | 1.74x | 43%
6 | 19001 | 2.02x | 51%
7 | 22296 | 1.72x | 43%
8 | 24149 | 1.59x | 40%


Surprisingly(to me at least), has preformance comparable to C++, but also a very powerful library of abstractions. The `2000` precision constant was found 
by trial and error such that no Fibonacci fails the `Fn = Fn-1 + Fn-2` test.


All tests were run on Linux 5.3.0-40-generic 18.04.1-Ubuntu i7-8565U CPU @ 1.80GHz (4 Core + HT)