I'll begin this by saying that I would have used python to generate some statistics/graphs
to analyse and see how they line up and what is the minimum text length to get a good guess.

Since my tool of choice(python) is forbidden, I have absolutely no remorse in using 
the hacker mindset of bash it until it breaks.

This game's rules are as follows:

* any tool/programming language may be used
* no perl or python
* no searching for the cypher itself
* find the key and the message

Thankfully, the lab kindly provides the encryption algorithm and language.
Breaking the code:

1. open google.com  
2. "letter substitution solver"
3. I'm feeling lucky (it's a very simple and common cypher, there's a good just I'll be lucky)
4. choose the English language
5. copy & paste
6. *Break Cypher*

#### Results

**Message**:
because the practice of the basic movements of kata is
the focus and mastery of self is the essence of
matsubayashi ryu karate do i shall try to elucidate the
movements of the kata according to my interpretation
based on forty years of study
it is not an easy task to explain each movement and its
significance and some must remain unexplained to give a
complete explanation one would have to be qualified and
inspired to such an extent that he could reach the state
of enlightened mind capable of recognizing soundless
sound and shapeless shape i do not deem myself the final
authority but my experience with kata has left no doubt
that the following is the proper application and
interpretation i offer my theories in the hope that the
essence of okinawan karate will remain intact

**Key**: mlvdrxopwzqhykjsfuibnecatg

Notes: 

* I am interested in cryptographic principles and structures, execution shouldn't matter.
I may use a computer or I could bang rocks to send/analyse a message, the information 
and underlying structure should be the same 
* Banning python as a language when any language/tool is allowed 
is unjustified, to me, banning a specific use of it seems way more appropriate
* Puzzles with arbitrary constraints are no fun, they should be united under a bigger vision 


