#!/usr/bin/env python3
import curses
import sys
import argparse
import random
from subprocess import Popen, PIPE
import time


parser = argparse.ArgumentParser()
parser.add_argument('-a', action='store_true', help='Start tty for Alice')
parser.add_argument('-b', action='store_true', help='Start tty for Bob')
args = parser.parse_args()

tty = None
if args.a is True:
    tty = 'a'
if args.b is True:
    tty = 'b'

# file descriptors:
# 0 < my tty
# 1 > my tty
# 2 > internet

if tty is not None:
    me = "Alice" if tty is 'a' else "Bob"
    other_tty = 'b' if tty is 'a' else "a"
    print('> ', flush=True, end='')

    # filter successful messages from other party and display them in this tty
    process = Popen(f"tail -f internet | grep --line-buffered '{other_tty}s ' | perl -pe '$|++;s/^{other_tty}s (.*?)\\n$/\\b\\b\\1\\n> /'",
                     shell=True, stdout=sys.stdout, stderr=sys.stderr)

    for line in sys.stdin:
        print('> ', flush=True, end='')
        transmitted = random.choice(['s', 'f'])  # success of failure
        print(tty + transmitted + ' ' + line, file=sys.stderr, flush=True)

    exit(0)


def main(scr, *args):
    # print('Initializing the interwebz...', file=sys.stderr, flush=True)

    # screen constants
    left = 1
    right = scr.getmaxyx()[1] - 1
    bottom = scr.getmaxyx()[0] - 2
    # entropy vars
    total = right - left
    alice = 0
    bob = total - 1

    block = [1] * (total + 1)

    # colors
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_WHITE)  # entropy
    curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_GREEN)  # used
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_YELLOW)  # being used
    curses.init_pair(4, curses.COLOR_WHITE, curses.COLOR_RED)  # dead

    # Setup
    curses.curs_set(False)
    scr.border(0)
    scr.addstr(1, 1, 'Common block of entropy:', curses.A_BOLD)
    scr.addstr(3, 1, 'Alice', curses.A_BOLD)
    scr.addstr(3, right - 3, 'Bob', curses.A_BOLD)

    scr.addstr(bottom - 2, 1, 'Legend:', curses.A_BOLD)
    scr.addstr(bottom, 2, ' ', curses.color_pair(1))
    scr.addstr(bottom, 4, 'Unused entropy', curses.A_NORMAL)
    scr.addstr(bottom, 21, ' ', curses.color_pair(2))
    scr.addstr(bottom, 23, 'Used', curses.A_NORMAL)
    scr.addstr(bottom, 30, ' ', curses.color_pair(3))
    scr.addstr(bottom, 32, 'Processing', curses.A_NORMAL)
    scr.addstr(bottom, 45, ' ', curses.color_pair(4))
    scr.addstr(bottom, 47, 'Wasted', curses.A_NORMAL)


    for i in range(left, right):
        scr.addstr(4, i, ' ', curses.color_pair(block[i]))

    scr.refresh()

    internet = open('internet', 'w+')

    while 1:
        where = internet.tell()
        line = internet.readline()
        if not line:
            time.sleep(2)
            internet.seek(where)
            continue

        line = line.strip()

        to_process = None

        msg_len = len(line) - 3
        msg = ""

        if msg_len > 0:
            msg = line[3:]

        if line.startswith('as'):
            block[alice:alice+msg_len] = [3] * msg_len
            to_process = alice
            alice += msg_len
        elif line.startswith('af'):
            block[alice:alice+msg_len] = [4] * msg_len
            alice += msg_len
        elif line.startswith('bs'):
            block[bob:bob+msg_len] = [3] * msg_len
            to_process = bob
            bob -= msg_len
        elif line.startswith('bf'):
            block[bob:bob+msg_len] = [4] * msg_len
            bob -= msg_len

        if alice > total/2 or bob < total/2:
            scr.addstr(5, total//2-6, 'Out of entropy', curses.A_BOLD)

        for i in range(left, right):
            scr.addstr(4, i, ' ', curses.color_pair(block[i]))

        scr.refresh()

        if to_process is not None:
            scr.addstr(7, 1, f'Processing: {msg}', curses.A_NORMAL)
            scr.refresh()

            time.sleep(2)
            block[to_process:to_process+msg_len] = [2] * msg_len

            scr.addstr(7, 1, ' ' * total, curses.A_NORMAL)
            for i in range(left, right):
                scr.addstr(4, i, ' ', curses.color_pair(block[i]))
            scr.refresh()


curses.wrapper(main)
print("done.")
