# Lab 11

It's been an interesting challenge to get this simulation working inside ttys

## Demo
### Execution commands ([full size](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/exec.png))
![Execution commands](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/resized_exec.png)
### Cleanly initialized ([full size](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/clean.png))
![Cleanly initialized](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/resized_clean.png)
### Encrypting message ([full size](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/processing.png))
![Encrypting message](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/resized_processing.png)
### Received ok ([full size](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/ok.png))
![Received ok](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/resized_ok.png)
### Transmission error ([full size](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/fault.png))
![Transmission error](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/resized_fault.png)
### Out of entropy ([full size](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/out_of_entropy.png))
![Out of entropy](https://bitbucket.org/FallenEmpyrean/pp/raw/e0a1d321a5a26216e313de960bc343ec87156aef/lab11/screenshots/resized_out_of_entropy.png)

