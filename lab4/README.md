# Lab 4 Report

### Question 1
Technically yes, it should be possible to create a new JVM and communicate in a way
similar to fork, but students like me would be put off by its verbosity and forced OOP
concepts. I am not familiar with how prolog handles multithreading/multiprocessing, but
judging from the fact that we study this language in this PP course, I will say that yes,
it's possible somehow.

### Question 2
When the program splits at the first fork, both processes will execute the following
forks and printf commands, same hold every further split in this tree of processes which
grows like 2^forks. I do not see out of order printing as unicode prevents me from
compiling the code, but it's probably because the main process doesn't finish last and
the console thinks that the whole execution is done and prints the prompt while other
processes are running. If this is the case, the parent should simply wait its children
to finish before exiting.

## Foreword

The interpretability of the following results depend heavily on findings in lab3. It 
is highly recommended to read `lab3/README.md` first

## Finding perfect numbers

The domain is split into equal continuous intervals. The following execution times were 
recorded(4-6 runs each, depending on variance):

Processes | Execution time (ms) | Speedup | Efficiency
---|---|---|---
1 | 20417 | 1x | 25%
2 | 18071 | 1.12x | 28%
3 | 14662 | 1.39x | 34%
4 | 13150 | 1.55x | 38%
5 | 15157 | 1.35x | 33%
6 | 8291  | 2.46x | 61%
7 | 13614 | 1.5x  | 37%
8 | 16028 | 1.27x | 31%

Lessons learnt:
* Do not use odd numbers as number of processes.
* Leave a few threads to the OS/audio & video playback.

Now let's load balance!

## Load balancing

The following execution times were 
recorded(4-6 runs each, depending on variance):

Processes | Execution time (ms) | Speedup | Efficiency
---|---|---|---
1 | 21353 | 1x | 25%
2 | 13101 | 1.62x | 40%
3 | 6137  | 3.48x | 87%
4 | 16235 | 1.31x | 32%
5 | 10560 | 2.02x | 50%
6 | 11047  | 1.93x | 48%
7 | 3445 | 6.1x  | 154%
8 | 3156 | 6.76x | 169%

Lessons learnt:
* Predicting parallel performance is like trying to predict the wheather:
you start by learning the seasons and think you get a pretty solid prediction,
until you read a snow forecast while drinking a cool lemonade on a sunny day.
* Having tea breaks between runs seems to give a ~3.56x speedup.
* Odd numbers are not bad.          

## PI Monte Carlo

The following execution times were 
recorded(1 run each, using the tea method):

Processes | Execution time (ms) | Speedup | Efficiency
---|---|---|---
1 | 1716 | 1x | 25%
2 | 642 | 2.67x | 66%
3 | 590  | 2.90x | 72%
4 | 515 | 3.33x | 83%
5 | 298 | 5.75x | 143%
6 | 264  | 6.50x | 162%
7 | 235 | 7.30x  | 182%
8 | 229 | 7.49x | 187%

Lessons learnt:
* Some say astrology is a hoax, I say it's just misunderstood patterns. Just as
drinking tea does not directly speed up the computer, neither does Saturn bring
breakthroughs in my life, and yet there is a clear correlation between the 
amount of tea that I drink and the obtained speedup.


All tests were run on Linux 5.3.0-40-generic 18.04.1-Ubuntu i7-8565U CPU @ 1.80GHz
       