# Project Structure

## Course Questions

The answers will be found in `Courses.md`, a splitting of that file might occur in case of 
an excess of questions to prevent scrolling.

## Labs
 
**Each lab** will have a **folder** for **all work** relevant to it. The specific configurations
will not be documented unless they have a relevant impact, as such they will only be found
the the specific commit tree.

The **lab folder** will have a `README.md` file in which **lab questions** are answered and 
**results** are **documented**. It will also contain all **source code** files except for
the comands I type directly into an interpreter(python, bash, etc.)

## Root folder

I will work in the root folder for each lab and upload everything just as it was when 
I run the tests, unless there are versions of interest, all commit will be squashed under a single one.
All work in here will be overridden by future labs. Historical copies
can be found in the lab's commit source tree and in the lab's folder.

Please note that generated files will not be included if they exceed 100MB. 