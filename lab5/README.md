
`std::thread` compiles into pthread anyway on linux, so I'm going to use the nice abstraction. The work is 
load balanced, each variant is run once.

Processes | Execution time (ms) | Speedup | Efficiency
---|---|---|---
1 | 61256 | 1x | 25%
2 | 65673 | 0.93x | 23%
3 | 48423  | 1.26x | 31%
4 | 48822 | 1.25x | 31%
5 | 44075 | 1.38x | 34%
6 | 48409 | 1.26x | 31%
7 | 51892 | 1.18x  | 29%
8 | 44198 | 1.38x | 34%


### Amdahl’s law
 this application has no thread inter-dependencies so it should be 100% parallelizable
(ignoring the thread creation/resynchronization with the main thread), obviously in practice there are
other factors which impact the execution time more than the theory predicts. Caching, cooling and 
OS-specific algorithms have a huge impact which is not taken into account by Amdahl which is more like 
an upper theoretical bound. I find meaniningless a graph of such results as it's more like a 
noisy square wave rather than anything elegant and mathematical. 

##### Final thoughts
Amdahl made a very good specific observation about a complex system, however being so simple it can be 
no more than a heuristic when it comes to approximating the real function. We shall wait for a 
general artificial intelligence which can teach us the true complexity in getting the best execution
time prediction.