#include <iostream>
#include <chrono>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <sys/wait.h>
#include <sys/mman.h>
#include <thread>

void *shalloc(size_t size) {
    int protection = PROT_READ | PROT_WRITE;
    int visibility = MAP_SHARED | MAP_ANONYMOUS;
    return mmap(NULL, size, protection, visibility, -1, 0);
}

using std::vector;
using std::cout, std::cin;

void *buffy;
int task = -1, mobsize = -1;
bool global_time = false;
//region timer
std::chrono::high_resolution_clock::time_point _time_start;

void timer_start() {
    _time_start = std::chrono::high_resolution_clock::now();

}

int timer_stop() {
    std::chrono::high_resolution_clock::time_point time_stop = std::chrono::high_resolution_clock::now();
    return static_cast<int>(std::chrono::duration_cast<std::chrono::microseconds>(time_stop - _time_start).count()) /
           1000;
}
//endregion


bool isPrime(int x) {

    for (int i = 2; i < sqrt(x); i++)
        if (x % i == 0)
            return false;

    return true;
}

bool isPerfect(int x) {

    int sum = 1;
    for (int i = 2; i < x; i++)
        if (x % i == 0)
            sum += i;

    return sum == x;

}

void findPerfects(int a, int b, int skip) {
    vector<int> perfects;
    perfects.reserve(b - a);

    for (int i = a; i < b; i += skip) {
        if (isPerfect(i))
            perfects.push_back(i);
    }

    if (true) {
        for (auto i : perfects)
            printf("%d ", i);
        printf("\n");
    }
}

int countDigits(int x){
    int ret = 1;
    while(x > 9) x/=10, ret++;
    return ret;
}

int decPow(int x){
    int ret = 1;
    while (x > 0) x/=10, ret *= 10;
    return ret;
}

bool isCircular(int x){
    int not_x = 0;
    int digits = countDigits(x);
    for (int i = 0; i < digits; i++){
        if(!isPrime(x + not_x))
            return false;

        not_x /= 10;
        not_x = x%10 * decPow(digits - 1);
        x /= 10;
    }
    return true;
}

void findCircularPrimes(int a, int b, int skip){
    vector<int> circlulars;
    circlulars.reserve(b-a);

    for (int i = a; i < b; i += skip) {
        if (isCircular(i))
            circlulars.push_back(i);
    }

    if (true) {
        for (auto i : circlulars)
            printf("%d ", i);
        printf("\n");
    }

}

void calculatePI_MonteCarlo(size_t N, int start, int stop, int child) {
    srand(time(NULL));

    unsigned int seed = time(NULL);
    int total = N;
    int inside = 0;

    const long long ONE = (long long) RAND_MAX * RAND_MAX;

    for (size_t i = start; i < stop; i++) {
        long long x = rand_r(&seed);//rand();
        long long y = rand_r(&seed);//rand();

        if (x * x + y * y < ONE)
            inside++;
    }

    double pi = 4.0 * inside / total;

    ((double *) buffy)[child] = pi;
}

void getI(int from, int to, int split, int i, int *start, int *end) {
    *start = from + i * (to - from) / split;
    *end = std::min(from + (i + 1) * (to - from) / split, to);
}


void work_balancer(int i) {

    printf("starting task ");


    if (task == 1) {
        int start, end;
        getI(2, 100000, mobsize, i, &start, &end);
        printf("#%d [%d, %d)\n", i, start, end);
        findPerfects(start, end, 1);

    } else if (task == 2) {
        printf("#%d [%d, %d, ... %d)\n", i, 2 + i, 2 + i + mobsize, 100000);
        findPerfects(2 + i, 100000, mobsize);

    } else if (task == 3) {
        int start, end;
        getI(0, 100000000, mobsize, i, &start, &end);
        printf("#%d [%ul, %ul)\n", i, start, end);
        calculatePI_MonteCarlo(100000000, start, end, i);

    } else if (task == 4) {
        printf("#%d [%d, %d, ... %d)\n", i, 2 + i, 2 + i + mobsize, 40000000);
        findCircularPrimes(2 + i, 40000000, mobsize);

    }
}

void collect() {
    if (task == 3) {
        double *slices = (double *) buffy;

        double pie = 0.0;
        for (int i = 0; i < mobsize; i++)
            pie += slices[i];

        printf("PI: %f \n", pie);
    }
}

void pitchforks_and_torches() {

    using std::vector, std::thread;

    timer_start();
    buffy = shalloc(256);
    vector<thread> threads;

    for (int i = 0; i < mobsize; i++)
        threads.push_back(thread(work_balancer, i));


    for (int i = 0; i < mobsize; i++)
        threads[i].join();

    collect();
    printf("Execution time %d ms\n", timer_stop());
}

void bare_main() {
    findCircularPrimes(2, 1000000, 1);

    //findPerfects(2, 100000, 1);

    /*
    generateEntropy(1000000000);
    global_time = true;
    calculateEntropy(0, 1000000000);
     */

    //calculatePI_MonteCarlo(100000000, 0, 100000000);


    std::cout << "Ze end!" << std::endl;
}

int main(int argc, char **argv) {

    if (argc == 1)
        bare_main();
    else if (argc == 3) {

        sscanf(argv[1], "%d", &task);
        sscanf(argv[2], "%d", &mobsize);

        global_time = true;

        pitchforks_and_torches();

    } else printf("Bad parameters.\n");

    return 0;
}
