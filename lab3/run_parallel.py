#!/usr/bin/env python3
from subprocess import Popen, PIPE

exe = 'cmake-build-release/pp'
#exe = 'echo'
interval = [0, 400000000]
instances = 8
task = 3

work_per_chunk = (interval[1] - interval[0])//instances

linear = [ [interval[0] + work_per_chunk * i,
            interval[0] + work_per_chunk * (i+1)]
            for i in range(instances) ]

custom = [[1,10],[11, 100]]

subintervals = linear

commands = [[exe, str(task), str(s[0]), str(s[1])] for s in subintervals]

procs = [ Popen(i, stdout=PIPE) for i in commands ]

result = 0
time = 0.0
for p in procs:
   p.wait()
   output = ""
   for line in p.stdout.readlines():
       output += str(line, "utf-8")
   print(f'Program output: {output}')
  
   words = output.replace("\n", " ").split(" ")
  
   time = max(time, int(words[0])/1000)
   #result += 1/7 * float(words[1])

print(f'Result {result} was calculated in {time} seconds')
