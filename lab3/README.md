# Lab 3 Report

## 1. Finding perferct numbers

When computing all **perfect numbers** in **[2, 1000000]** the **execution** of the single process variant **lasted** for **more** 
than _one cup of tea_. A **speedup** was required to find all perfect numbers and compute meaningful averages. Thankfully, 
the [oeis perfect number sequence](https://oeis.org/A000396) reveals the the **largest** one is **8128**, so the **interval**
got **reduced** to **[2, 100000]**. 

The following table contains the speedups achieved by splitting the task into 1-8 processes on a laptop with hyperthreading.
Note that the execution time does not measure process management overhead and console interactions.

| Processes | Executiion time (avg(max) s) | Speedup
|------------|----|-----
| 1 (serial) | 19 | 1x
| 2          | 16 | 1.18x
| 3          | 15 | 1.26x
| 4          | 12 | 1.58x
| 8          | 10 | 1.9x

The speedup is quite noticeable, however the workload splitting is not equal among processes. These are the times for
for an 8-way split:

| Interval start | Interval end | Execution time (ms)
|------|-------|-----
|2     | 12501 | 389
|12501 | 25000 | 1175
|25000 | 37499 | 2326
|37499 | 49998 | 2364
|49998 | 62497 | 4825
|62497 | 74996 | 7441
|74996 | 87495 | 8787
|87495 | 99994 | 9844

An even distribution may cut the time in half bringing the maximum speedup to about 3.8 which is pretty close
to the my hardware limit of 4 cores. Note that a truly even distribution is quite difficult to estimate
without empirical data, as the number of operations varies from number to number and silent optimizations may be 
running under the hood.

## 2. Entropy estimation

The problem of fast file I/O begins with what function to use to perform the operations. Giving the vector to`fstream` 
seems to provide competitive performance based on 
[[1]](https://lemire.me/blog/2012/06/26/which-is-fastest-read-fread-ifstream-or-mmap/)
[[2]](https://stackoverflow.com/questions/11563963/writing-a-binary-file-in-c-very-fast)
[[3]](https://stackoverflow.com/questions/1693089/fastest-way-to-write-large-stl-vector-to-file-using-stl)
which averages 0.3s / 1.7s (r/w) for the 1GB sequence

The following table contains the speedups:

| Processes | Execution time (avg(max) s) | Speedup
|------------|----|-----
| 1 (serial) | 0.37 | 1x
| 2          | 0.22 | 1.68x
| 3          | 0.17 | 2.17x
| 4          | 0.15 | 2.46x
| 8          | 0.15 | 2.46x

The bottleneck seems to be the file i/o which prevents linear speedup. The computation after synchronization is
negligible for practical purposes, even more so because the work is equally distributed among processes. 

## 3. PI Monte Carlo

This first batch of test was done with the clasic `rand()` the following table contains the speedups:

| Processes | Executiion time (avg w/ long breaks - avg w/ continuous execution)  | Speedup
|------------|----|-----
| 1 (serial) | 7.4 - 12.5 | 1x
| 2          | 3.3 - 8.1 | 2.42x
| 4          | 2.2 - 8.1 | 3.36x
| 8          | 2.1 - 8.0 | 3.52x

A **very curious behaviour** seemed to dominate the behaviour of this programs' performance: **the longer I waited 
between executions**, the **faster** the **execution** time got. This behaviour was observed no matter the process
number.  Furthermore, there was **no correlation** between **execution time** and the result of 
`cat /proc/sys/kernel/random/entropy_avail`. **I am baffled**, as it seems to go against the usual behaviour of caching:
things should speed up when localized in time. I would love to find out the underlying cause. 

Unfortunately for the lesson, the order of magnitude of the above behaviour exceeds by far any other factors which 
influence a single run. Moreover, the **execution time** of a **multi-processes version** can **exceed** the slow average of the 
**serial version**(e.g. ~15s for two threads), although not very often. However, another curious behaviour shows up when
the program is run continuously: the average execution time gets stuck at ~8s no mater the increase in threads above 2. 

After switching to **rand_r()** the results are dramatically better, but just as unsettling:

| Processes | Executiion time (avg(max) s) | Speedup
|------------|----|-----
| 1 (serial) | 3.4 - 6.0 | 1x
| 2          | 1.9 - 4.3 | 1.7x
| 4          | 1.2 - 4.7 | 2.8x
| 8          | 0.8 - 4.2 | 4.25x


All tests were run on Linux 5.3.0-40-generic 18.04.1-Ubuntu i7-8565U CPU @ 1.80GHz

