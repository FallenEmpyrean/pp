#include <iostream>
#include <chrono>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <fstream>

using std::vector;
using std::cout, std::cin;

bool global_time = false;
//region timer
std::chrono::high_resolution_clock::time_point _time_start;

void timer_start() {
    _time_start = std::chrono::high_resolution_clock::now();

}

int timer_stop() {
    std::chrono::high_resolution_clock::time_point time_stop = std::chrono::high_resolution_clock::now();
    return static_cast<int>(std::chrono::duration_cast<std::chrono::microseconds>(time_stop - _time_start).count()) /
           1000;
}
//endregion


bool isPrime(int x) {

    for (int i = 2; i < sqrt(x); i++)
        if (x % i == 0)
            return false;

    return true;
}

bool isPerfect(int x) {

    int sum = 1;
    for (int i = 2; i < x; i++)
        if (x % i == 0)
            sum += i;

    return sum == x;

}

void findPerfects(int a, int b) {
    vector<int> perfects;
    perfects.reserve(b - a);

    timer_start();
    for (int i = a; i <= b; i++) {
        if (isPerfect(i))
            perfects.push_back(i);
    }

    if (!global_time)
        printf("Perfects(%zu) time: %d ms \n", perfects.size(), timer_stop());
    else
        printf("%d\n", timer_stop());

    if (true) {
        for (auto i : perfects)
            printf("%d ", i);
        printf("\n");
    }
}

void findPrimes(int a, int b) {
    vector<int> primes;
    primes.reserve(b - a);

    timer_start();
    for (int i = a; i <= b; i++) {
        if (isPrime(i))
            primes.push_back(i);
    }

    printf("Primes(%zu) time: %d ms \n", primes.size(), timer_stop());

    if (false) {
        for (auto i : primes)
            printf("%d ", i);
        printf("\n");
    }
}


void calculateEntropy(size_t start, size_t stop) {
    using namespace std;

    size_t ent_size = stop - start;
    vector<char> seq(ent_size);

    //timer_start();

    ifstream infile("input.bin", ifstream::binary);
    infile.seekg(start, infile.beg);
    infile.read(&seq[0], ent_size);
    infile.close();

    //printf("%d ms\n", timer_stop());

    timer_start();
    const int CHUNK_SIZE = 8;
    const int LUT_SIZE = pow(2, CHUNK_SIZE);
    vector<int> lut(LUT_SIZE);

    for (int i = 0; i < LUT_SIZE; i++) {
        int ones = 0;
        for (int k = 0; k < CHUNK_SIZE; k++)
            ones += (i & (1 << k)) == 0 ? 0 : 1;
        lut[i] = ones;
    }

    size_t ones = 0;
    for (size_t i = 0; i < ent_size; i++)
        ones += lut[seq[i]];


    size_t total = ent_size * CHUNK_SIZE;
    size_t zeroes = total - ones;

    if (global_time) {
        printf("%d\n", timer_stop());
        printf("%lu %lu\n", ones, total);
    } else {
        double entropy = -((double) ones / total) * log2((double) ones / total) -
                         ((double) zeroes / total) * log2((double) zeroes / total);

        printf("Sequence entropy(%f): %d ms \n", entropy, timer_stop());
    }

}

void generateEntropy(size_t ent_size) {
    using namespace std;

    vector<char> seq(ent_size);

    srand(time(NULL));
    for (int i = 0; i < ent_size; i++)
        seq[i] = rand() % 256;


    ofstream outfile("input.bin", ofstream::binary);
    outfile.write(&seq[0], seq.size());
    outfile.close();

}

void calculatePI_integral(int N) {
    double pi = 0.0;

    timer_start();

    for (int i = 0; i < N; i++)
        pi += 4.0 / (1 + pow((i + 0.5) / N, 2));

    pi /= N;

    printf("PI integral approx(%f): %d ms \n", pi, timer_stop());

}

void calculatePI_MonteCarlo(int N, int start, int stop) {
    timer_start();
    srand(time(NULL));

    unsigned int seed = time(NULL);
    int total = N;
    int inside = 0;

    const long long ONE = (long long) RAND_MAX * RAND_MAX;

    for (int i = start; i < stop; i++) {
        long long x = rand_r(&seed);//rand();
        long long y = rand_r(&seed);//rand();

        if (x * x + y * y < ONE)
            inside++;
    }

    double pi = 4.0 * inside / total;

    if (global_time)
        printf("%d\n%f\n", timer_stop(), pi);
    else
        printf("PI Monte Carlo approx(%f): %d ms \n", pi, timer_stop());

}

void param_main(int task, size_t a, size_t b) {
    if (task == 1)
        findPerfects(a, b);
    else if (task == 2)
        calculateEntropy(a, b);
    else if (task == 3)
        calculatePI_MonteCarlo(400000000, a, b);

}

void bare_main() {
    //findPerfects(2, 100000);

    /*
    generateEntropy(1000000000);
    global_time = true;
    calculateEntropy(0, 1000000000);
     */

    calculatePI_MonteCarlo(100000000, 0, 100000000);


    return;

    findPrimes(100, 100000);
    calculateEntropy(0, 1000000000);
    calculatePI_integral(10000000);
    calculatePI_MonteCarlo(0, 10000000, 10000000);

    std::cout << "Ze end!" << std::endl;
}

int main(int argc, char **argv) {

    if (argc == 1)
        bare_main();
    else if (argc == 4) {

        int task;
        size_t a, b;
        sscanf(argv[1], "%d", &task);
        sscanf(argv[2], "%lu", &a);
        sscanf(argv[3], "%lu", &b);

        global_time = true;
        param_main(task, a, b);


    } else printf("Bad parameters.\n");

    return 0;
}
