#include <iostream>
#include <chrono>
#include <vector>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <sys/wait.h>
#include <thread>
#include <boost/math/special_functions/pow.hpp>
#include <cmath>
#include <omp.h>

using std::vector;
using std::cout, std::cin, std::ifstream, std::ofstream;


std::chrono::high_resolution_clock::time_point _time_start;

void timer_start() {
    _time_start = std::chrono::high_resolution_clock::now();

}

int timer_stop() {
    std::chrono::high_resolution_clock::time_point time_stop = std::chrono::high_resolution_clock::now();
    return static_cast<int>(std::chrono::duration_cast<std::chrono::microseconds>(time_stop - _time_start).count()) /
           1000;
}

void calculatePI_integral(uint64_t N) {
    double pi = 0.0;

    timer_start();

#pragma omp parallel for reduction(+:pi) private(i)
    for (uint64_t i = 0; i < N; i++)
        pi += 4.0 / (1 + pow((i + 0.5) / N, 2));

    pi /= N;

    printf("PI integral approx(%f): %d ms \n", pi, timer_stop());

}

void bare_main() {

    calculatePI_integral(8000000000);

    std::cout << "Ze end!" << std::endl;
}

int main(int argc, char **argv) {

    if (argc == 1) {
        cout<<"threads="<<omp_get_num_threads()<<"\n";
        bare_main();
    }
    else if (argc == 2) {
        int threads;
        sscanf(argv[1], "%d", &threads);
        omp_set_dynamic(0);
        omp_set_num_threads(threads);
        cout<<"threads="<<omp_get_num_threads()<<"\n";
        cout << "Starting " << threads << " threads...\n";
        calculatePI_integral(8000000000);
    } else printf("Bad parameters.\n");

    return 0;
}
