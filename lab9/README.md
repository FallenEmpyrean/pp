
Prolog has truly gained my respect as a language.

Threads | Execution time (ms) | Speedup | Efficiency
---|---|---|---
1 (no omp) | 42987 | 1x | 25%
1 | 44618 | ~1x | ~25%
2 | 27662 | 1.55x | 38%
3 | 24810 | 1.73x | 43%
4 | 24067 | 1.78x | 44%
5 | 23021 | 1.86x | 46%
6 | 23389 | 1.83x | 45%
7 | 21772 | 1.97x | 49%
8 | 21572 | 1.99x | 49%
auto | 23645 | 1.81x | 45%


As always, other factors seem to have a bigger impact on the time, rather than thread count
above 2-3, the best time being 19370ms which happened only once on 3 threads
(still far from the theoretical 10000ms). 
I may misuse this function `omp_get_num_threads`, but it always returns `1` regardless
of how many threads I see in my task manager. `omp_set_num_threads` was rather 
inconsistent too regarding times.

This being the last multithreading lab(hopefully, go crypto!), 
I would like to add that these results strengthened
my beliefs in empirical methods. Simple theoretical models are cute, however they do not hold
when faced with the complexity of the real world. Without a good tool to fit them to practice, 
they become just rough heuristics which are nice to bear in mind.comm

All tests were run on Linux 5.3.0-40-generic 18.04.1-Ubuntu i7-8565U CPU @ 1.80GHz (4 Core + HT)