#include <iostream>
#include <chrono>
#include <vector>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <sys/wait.h>
#include <thread>
#include <boost/math/constants/constants.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/cpp_bin_float.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/number.hpp>
#include <boost/math/special_functions.hpp>

using std::vector;
using std::cout, std::cin, std::ifstream, std::ofstream;
using boost::multiprecision::number;
using boost::multiprecision::backends::cpp_bin_float;
using boost::multiprecision::backends::cpp_int_backend;
using boost::multiprecision::cpp_integer_type;
using boost::multiprecision::cpp_int_check_type;

typedef number<cpp_bin_float<8192>> zfloat;
typedef number<cpp_int_backend<8192, 8192, cpp_integer_type::unsigned_magnitude, cpp_int_check_type::unchecked, void> > zint;

int task = -1, mobsize = -1;
bool global_time = false;
//region timer
std::chrono::high_resolution_clock::time_point _time_start;

void timer_start() {
    _time_start = std::chrono::high_resolution_clock::now();

}

int timer_stop() {
    std::chrono::high_resolution_clock::time_point time_stop = std::chrono::high_resolution_clock::now();
    return static_cast<int>(std::chrono::duration_cast<std::chrono::microseconds>(time_stop - _time_start).count()) /
           1000;
}
//endregion


zfloat phi, sq5;
vector<vector<zint>> fibs;

void calculateFib(int a, int b, int skip, int t) {
    namespace mp = boost::multiprecision;

    for (int i = a; i < b; i += skip) {
        zint current = zint(mp::floor(mp::pow(phi, i) / sq5 + 0.5));
        //cout << i << "\n";

        if (i > 10) {
            zint p1 = zint(mp::floor(mp::pow(phi, i - 1) / sq5 + 0.5));
            zint p2 = zint(mp::floor(mp::pow(phi, i - 2) / sq5 + 0.5));
            if((p1 + p2) != current) {
                cout << "Computation fault at: F(" << i << ") = " << current << "\n";
                cout << "Error is: " << current - (p1 + p2) << "\n";
                break;
            }
        }

        //cout << current << "\n";
        fibs[t].push_back(zint(mp::floor(mp::pow(phi, i)/sq5+0.5)));

    }
}

void getI(int from, int to, int split, int i, int *start, int *end) {
    *start = from + i * (to - from) / split;
    *end = std::min(from + (i + 1) * (to - from) / split, to);
}


void prepare() {
    if (task == 1) {
        namespace mp = boost::multiprecision;
        for (int k = 0; k < mobsize; k++)
            fibs.push_back(vector<zint>());
        sq5 = mp::sqrt(zfloat(5));
        phi = (1 + sq5) / 2;
    }
}

void work_balancer(int i) {

    printf("starting task ");

    if (task == 1) {
        printf("#%d [%d, %d, ... %d)\n", i, 2 + i, 2 + i + mobsize, 1500);
        calculateFib(1 + i, 1500, mobsize, i);
    }
}

void collect() {
    if (task == 1) {

        ofstream f("fibs.txt");

        int batch = 1500 / mobsize;
        int fi = 1;
        for (int i = 0; i < batch; i++) {
            for (int offset = 0; offset < mobsize && i < fibs[offset].size(); offset++) {
                f << "F(" << fi << ") = " << fibs[offset][i] << "\n";
                fi++;
            }
        }
    }
}

void pitchforks_and_torches() {

    using std::vector, std::thread;

    timer_start();
    prepare();
    vector<thread> threads;

    for (int i = 0; i < mobsize; i++)
        threads.push_back(thread(work_balancer, i));


    for (int i = 0; i < mobsize; i++)
        threads[i].join();

    printf("Execution time %d ms\n", timer_stop());
    collect();
}

void bare_main() {
    mobsize = 1;
    namespace mp = boost::multiprecision;

    for (int k = 0; k < mobsize; k++)
        fibs.push_back(vector<zint>());

    sq5 = mp::sqrt(zfloat(5));
    phi = (1 + sq5) / 2;
    calculateFib(1, 1680, 1, 0);

    //findPerfects(2, 100000, 1);

    /*
    generateEntropy(1000000000);
    global_time = true;
    calculateEntropy(0, 1000000000);
     */

    //calculatePI_MonteCarlo(100000000, 0, 100000000);


    std::cout << "Ze end!" << std::endl;
}

int main(int argc, char **argv) {

    if (argc == 1)
        bare_main();
    else if (argc == 3) {

        sscanf(argv[1], "%d", &task);
        sscanf(argv[2], "%d", &mobsize);

        global_time = true;

        pitchforks_and_torches();

    } else printf("Bad parameters.\n");

    return 0;
}
