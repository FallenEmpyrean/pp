
Java? BigNumbers? What a great opportunity to study boost's multiprecision library! Domain is loadbalanced, each 
variant is run once.

Processes | Execution time (ms) | Speedup | Efficiency
---|---|---|---
1 | 85418 | 1x | 25%
2 | 56280 | 1.51x | 37%
3 | 32225  | 2.65x | 66%
4 | 34432 | 2.48x | 62%
5 | 45585 | 1.87x | 46%
6 | 39067 | 2.18x | 54%
7 | 37912 | 2.25x  | 56%
8 | 42450 | 2.01x | 50%


Surprisingly, this lab is the most well-behaved from all so far. The size of zint and zfloat were chosen 
by trial and error such that no Fibonacci fails the `Fn = Fn-1 + Fn-2` test.


All tests were run on Linux 5.3.0-40-generic 18.04.1-Ubuntu i7-8565U CPU @ 1.80GHz (4 Core + HT)