# Course 4

### Question 1

The fact that Java took almost 20 years to add a function(method) pointer into it's specification and I'm still 
forced to create/use N **AbstractSingletonProxyFactoryBean**([Spring source link](https://docs.spring.io/spring/docs/2.5.x/javadoc-api/org/springframework/aop/framework/AbstractSingletonProxyFactoryBean.html)) 
classes according to its design principles for cases which don't justify their added conceptual complexity is enough 
of a deterent for me. Java and JIT can do amazing things, but there is a repelling aura all around.

### Question 2

The main thread is usually made by the operating system/libraries to have more privileges, I've encoutered all sorts 
of random errors of seemingly working code and then found out that that certain operation can only be done from a 
ceratain thread. These errors usually involved a graphics context(GPU or UI) or some other resource which was locked 
to a specific thread(usually main).

### Question 3 
 
... Everything is an object.
 
 
 # Course 5
 
 ### Question 1
 
I actually believe that doing as many things as possible in parallel is the way to go ..as long as the drawbacks
don't become greater than the reward. 
* Example 1: Sharing an object. Two friends see an awesome facebook video in which two people perform this 
"well choreographed dance" of hitting and object and turning it by hand to flatten it. Everything is a continous motion
and they both do the same in in parrallel, but simply out of phase. Now the X and Y viewers find it really cool and
wish to perform the same thing, however when they try they end up with broken fingers.

    What happened? That parallelization worked on certain assumptions which X and Y didn't respect because
    they didn't truly understand. Synchronization must always be taken seriously.
    
* Example 2: A factory, made of two sequential sections, bought a new second assembly line to double the throughput of 
its first section. What it didn't realize was that due to the newfound stress on the second section, manufacturing 
defects begun to show up which required people from the second section to repair them, thus halting the production
and lowering the average throughput even less than the thoughput before the upgrade.

    What happened? Parallelization raised the complexity of the system and unforseen problems emerged. Every change's 
    effects should be predicted to the best of one's abilities to make sure that there are no gaps in his knowledge,
    if there are well-controlled test should be performed to gain knowledge before attempting to make the big change.

 ### Question 2
 
1. 20
2. semaphore
3. threads after P
4. the semaphore's "space" of running threads
5. the synchonized resource
6. a race condition implies either that a 20-semaphore is not the proper synchronization mechanism(perhaps a mutex?) or
that the threads are running bad code   
 
 ### Question 3 
  
Flavour is in the eye(mouth) of the beholder. If a C library has N wrappers on top of it and I get to use it in my 
favourite language and in my favourite paradigm and also meet my non-functional requirement then it comes my 
favourite language's flavour. In practice, only effects matter and if in my use case it looks and quacks like
a duck, it's a duck.  

### Question 4

1. OpenMP will choose an optimum number of threads by default, which is usually the number of cores.
2. * OpenMP directives can be treated as comments if OpenMP is not available
   * Ease of parallelizing a existing code
   * OpenMP codes cannot be run on distributed memory computers (exception is Intel's OpenMP)
   * Often has lower parallel efficiency than a hand-crafted solution
3. It depends, somewhere between the number of physical/logical cores. 
Yes: `omp_set_num_threads(std::thread::hardware_concurrency());` (C++11)

### Question 5
I do not enjoy Microsoft-related problems, especially searching in menus for a flag only to find out that it got set only
for the debug mode. I live in linux now. I vim my `CMakeLists.txt` search the dependency
`find_package(OpenMP REQUIRED)` and link it `target_link_libraries( ... OpenMP::OpenMP_CXX)` to my project. Easy, stress-free.

  